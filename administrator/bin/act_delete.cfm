<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfimport 
	taglib =	"../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<!--- ****************************************** --->
<!--- Delete Records                             --->
<!--- ****************************************** --->
<cfquery name="thisE" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select code from #kakapo.table# where errorid = #val(url.errorid)#
</cfquery>
<cfloop query="thisE">
	<cfif FileExists("#kakapo.LogFolder#/variables/#thisE.Code#.cfm")>
		<cffile action="delete" file="#kakapo.LogFolder#/variables/#thisE.Code#.cfm" />
	</cfif>
	<cfif FileExists("#kakapo.LogFolder#/pages/#thisE.Code#.cfm")>
		<cffile action="delete" file="#kakapo.LogFolder#/pages/#thisE.Code#.cfm" />
	</cfif>
</cfloop>

<cfquery name="delete" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	delete from #kakapo.table# where errorid = #val(url.errorid)#
</cfquery>

<cfset session.msg	= "Record Deleted Successfully" />
<cflocation addtoken="no" url="index.cfm?page=#url.page#" />