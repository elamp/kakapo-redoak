
<cfparam name="form.file" default="">
<cfif len(form.file)>
	<cffile
		action 			= "upload"
		fileField 		= "form.file" 
		nameconflict	= "makeunique"
		destination 	= "#kakapo.LogFolder#\variables">
		<cfif not ListFindNoCase('xml',file.clientFileExt)>
			<cfset session.error	= "Import failed. Not XML file.">
			<cffile action = "delete" file = "#kakapo.LogFolder#\variables\#serverFile#">
		<cfelse>
			<cffile action="read" file="#kakapo.LogFolder#\variables\#serverFile#" variable="kakapoFile" />
			<cfif not IsXML(kakapoFile)>
				<cffile action = "delete" file = "#kakapo.LogFolder#\variables\#serverFile#">
				<cfset session.error	= "Import failed. Not valid XML file.">
			<cfelse>
				<cfset kakapoFile = XMLParse(kakapoFile)>
				<cfif not StructKeyExists(kakapoFile,'kakapo')>
					<cffile action = "delete" file = "#kakapo.LogFolder#\variables\#serverFile#">
					<cfset session.error	= "Import failed. Not valid import file.">
				<cfelse>
					<cfif not IsWDDX(kakapoFile.kakapo.query.XmlText)>
						<cffile action 	= "delete" file 	= "#kakapo.LogFolder#\variables\#serverFile#">
						<cfset session.error	= "Import failed. Not valid import file.">
					<cfelse>
						<cftransaction>
						<cfdump var="#kakapoFile#">
						<cfwddx action="wddx2cfml" input="#kakapoFile.kakapo.query.XmlText#" output="kakapoLog">
						<!--- create error id   --->
						<cfquery name="kakapo.i" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
							select max(errorid)+1 as mxerrorid from #kakapo.table#
						</cfquery>
						
						<cfset Errorid	= val(kakapo.i.mxerrorid)+1>
						<cfset Code		= CreateUUID()>
						<!--- insert the record --->
						<cfquery datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#" name="kakapo.add">
							insert into #kakapo.table#
							(line, etime, errorid, code, template,message,type)
							values
							(#val(kakapoLog.Line)#,#CreateODBCDateTime(now())#,#val(Errorid)#,
							<cfqueryparam
								value="#Code#"
								cfsqltype="CF_SQL_VARCHAR"
								maxlength="35">,
							<cfqueryparam
								value="#left(trim(kakapoLog.template),200)#"
								cfsqltype="CF_SQL_VARCHAR"
								maxlength="200">,
							<cfqueryparam
								value="#left(trim(kakapoLog.Message),500)#"
								cfsqltype="CF_SQL_VARCHAR"
								maxlength="500">,
							<cfqueryparam
								value="i.#kakapoLog.Type#"
								cfsqltype="CF_SQL_VARCHAR"
								maxlength="20">
								)
						</cfquery>
						
						<cftry>
							<cffile
								action 		= "write"
								file 		= "#kakapo.LogFolder#/variables/#Code#.cfm"
								output 		= "<cfabort />#kakapoFile.kakapo.vari.XmlText#">
							<cfcatch></cfcatch>
						</cftry>
						
						<cftry>
							<cffile
								action 		= "write"
								file 		= "#kakapo.LogFolder#/pages/#Code#.cfm"
								output 		= "<cfabort />#kakapoFile.kakapo.code.XmlText#">
							<cfcatch></cfcatch>
						</cftry>
						
						<cffile action = "delete" file = "#kakapo.LogFolder#\variables\#serverFile#">
						<cfset session.msg	= "Import Successful">
						</cftransaction>
					</cfif>
				</cfif>
			</cfif>			
		</cfif>
</cfif>