<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : Nov 19, 2011     mmm                                    --->
<!--- ********************************************************************* --->
<cfimport 
	taglib =	"../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<!--- *************************************************** --->
<!--- get the id list of all the records to memory        --->
<!--- *************************************************** --->
<cfquery name="master" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select bugid from #kakapo.table#
	where errorid = #val(url.errorid)#
	<cfif len(url.srch)>
		and template like '%#trim(url.srch)#%' or message like '%#trim(url.srch)#%'
	</cfif>
	order by
    <cfswitch expression="#url.order#">
        <cfcase value="viewed">		viewed	</cfcase>
		<cfcase value="status">		status	</cfcase>
        <cfcase value="template">	template</cfcase>
        <cfcase value="line">		line	</cfcase>
        <cfcase value="message">	message	</cfcase>
        <cfcase value="type">		type	</cfcase>
		<cfdefaultcase>				etime	</cfdefaultcase>
    </cfswitch>
    <cfswitch expression="#url.dir#">
        <cfcase value="0">
            asc
        </cfcase>
        <cfdefaultcase>
            desc
        </cfdefaultcase>
    </cfswitch>
</cfquery>

<cfsilent>
<cfset searchlist	= "#valuelist(master.bugid)#">
<cfinclude template="../system/common/grid_calc.cfm">
<!--- *************************************************** --->
<!--- get full details for current page records           --->
<!--- *************************************************** --->
<cfquery name="get" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select bugid as id, errorid, template,line,message, type
	from #kakapo.table#
	where <cfif listlen(searchlist)>bugid in (#searchlist#)<cfelse>errorid = 0</cfif>
	order by
    <cfswitch expression="#url.order#">
        <cfcase value="viewed">		viewed	</cfcase>
		<cfcase value="status">		status	</cfcase>
        <cfcase value="template">	template</cfcase>
        <cfcase value="line">		line	</cfcase>
        <cfcase value="message">	message	</cfcase>
        <cfcase value="type">		type	</cfcase>
		<cfdefaultcase>				etime	</cfdefaultcase>
    </cfswitch>
    <cfswitch expression="#url.dir#">
        <cfcase value="0">
            asc
        </cfcase>
        <cfdefaultcase>
            desc
        </cfdefaultcase>
    </cfswitch>
</cfquery>
</cfsilent>

<div id="breadcrumb"><a href="index.cfm">Home</a> <img src="images/garow.gif" align="middle" /> <cfoutput>#get.message#</cfoutput></div>

<!--- *************************************************** --->
<!--- Show Error Grid                                     --->
<!--- *************************************************** --->
<cfset titlelist = "&nbsp;|status,Template|template,Line|line,Message|message,Type|type,Time|etime">
<cfinclude template="../system/common/grid_header.cfm">
<cfoutput query="get">

<cfquery name="thisQ" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select viewed,status,etime from #kakapo.table# where bugid = #get.id#
</cfquery>

<tr id="#id#" <cfif val(thisQ.viewed)><cfif not currentrow mod 2>class="zibrablk"<cfelse>class="zibrawhite"</cfif><cfelse>class="highlight"</cfif> >
	<td><img src="images/#val(thisQ.status)#_stat.png" class="statbtn" /></td>
	<td class="linkme" title="#template#"><cfif len(template) gt 40 and listlen(template,'\/') gt 2>#listfirst(template,'\/')#\..\#listlast(template,'\/')#<cfelse>#template#&nbsp;</cfif></td>
	<td class="linkme">#line#</td>
	<td class="linkme">#message#</td>
	<td class="linkme">#lcase(type)#</td>
	<td class="linkme">#dateformat(thisQ.etime,'ddd mm/dd/yy')# #timeformat(thisQ.etime,'hh:mm tt')#</td>
</tr>
</cfoutput>
</table>
<cfinclude template="../system/common/grid_navi.cfm">