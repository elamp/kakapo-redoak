<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 27, 2011                                         --->
<!--- ********************************************************************* --->
<cfimport 
	taglib =	"../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<cfsilent>
<!--- *************************************************** --->
<!--- get the id list of all the records to memory        --->
<!--- *************************************************** --->
<cfquery name="master" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select max(errorid) as errorid from #kakapo.table#
	<cfif len(url.srch)>
		where template like '%#trim(url.srch)#%' or message like '%#trim(url.srch)#%'
	</cfif>
	group by errorid,template,message, type, line
	order by
    <cfswitch expression="#url.order#">
        <cfcase value="viewed">		viewed			</cfcase>
		<cfcase value="status">		max(status)		</cfcase>
        <cfcase value="template">	template		</cfcase>
        <cfcase value="line">		line			</cfcase>
        <cfcase value="message">	message			</cfcase>
        <cfcase value="ecount">		count(errorid)	</cfcase>
        <cfcase value="type">		type			</cfcase>
		<cfdefaultcase>				max(etime)		</cfdefaultcase>
    </cfswitch>
    <cfswitch expression="#url.dir#">
        <cfcase value="0">
            asc
        </cfcase>
        <cfdefaultcase>
            desc
        </cfdefaultcase>
    </cfswitch>
</cfquery>

<cfset searchlist	= "#valuelist(master.errorid)#">
<cfinclude template="../system/common/grid_calc.cfm">
<!--- *************************************************** --->
<!--- get full details for current page records           --->
<!--- *************************************************** --->
<cfquery name="get" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select max(bugid) as id, count(errorid) as ecount, max(errorid) as errorid, template,line,message, type
	from #kakapo.table#
	where <cfif listlen(searchlist)>errorid in (#searchlist#)<cfelse>errorid = 0</cfif>
	group by errorid,template,message, type, line
	order by
    <cfswitch expression="#url.order#">
        <cfcase value="viewed">		viewed			</cfcase>
		<cfcase value="status">		max(status)		</cfcase>
        <cfcase value="template">	template		</cfcase>
        <cfcase value="line">		line			</cfcase>
        <cfcase value="message">	message			</cfcase>
        <cfcase value="ecount">		count(errorid)	</cfcase>
        <cfcase value="type">		type			</cfcase>
		<cfdefaultcase>				max(etime)		</cfdefaultcase>
    </cfswitch>
    <cfswitch expression="#url.dir#">
        <cfcase value="0">
            asc
        </cfcase>
        <cfdefaultcase>
            desc
        </cfdefaultcase>
    </cfswitch>
</cfquery>

</cfsilent>

<!--- *************************************************** --->
<!--- Show Error Grid                                     --->
<!--- *************************************************** --->
<cfset titlelist = "&nbsp;|status,Count|ecount,Template|template,Line|line,Message|message,Type|type,Time|etime, ">
<cfinclude template="../system/common/grid_header.cfm">
<cfoutput query="get">

<cfquery name="thisQ" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select viewed,status,etime from #kakapo.table# where bugid = #get.id#
</cfquery>

<tr id="#id#" <cfif val(thisQ.viewed)><cfif not currentrow mod 2>class="zibrablk"<cfelse>class="zibrawhite"</cfif><cfelse>class="highlight"</cfif> >
	<td align="center"><img src="images/#val(thisQ.status)#_stat.png" class="statbtn" original-title="<cfswitch expression='#val(thisQ.status)#'><cfcase value='0'>New</cfcase><cfcase value='1'>Attending</cfcase><cfcase value='2'>Ignored</cfcase><cfcase value='3'>Fixed</cfcase></cfswitch>" /></td>
	<td class="ecounts"><cfif ecount gt 1><a href="index.cfm?action=incidence&errorid=#get.errorid#&id=#get.id#"><img original-title="View All Incidences" style="position:relative; vertical-align:middle" src="images/plusicon.gif" width="15" height="15" align="absmiddle" border="0" /></a> #ecount#
	<cfelse>
		<cfswitch expression="#listfirst(type,'.')#">
			<cfcase value="i"><img src="images/settingssml.png" class="alert" original-title="Imported Record" border="0" align="absmiddle" /></cfcase>
		</cfswitch>
	</cfif></td>
	<td class="linkme alert" original-title="#template#"><cfif len(template) gt 40 and listlen(template,'\/') gt 2>#listfirst(template,'\/')#\..\#listlast(template,'\/')#<cfelse>#template#&nbsp;</cfif></td>
	<td class="linkme">#line#</td>
	<td class="linkme">#message#</td>
	<td class="linkme">#lcase(listlast(type,'.'))#</td>
	<td class="linkme">#dateformat(thisQ.etime,'ddd mm/dd/yy')# #timeformat(thisQ.etime,'hh:mm tt')#</td>
	<td class="alert"><a href="javascript:conf('Delete Record','Are You sure You want to delete this record?','index.cfm?action=delete&errorid=#get.errorid#')"><img src="images/delete.png" style="cursor:pointer" border="0" /></a></td>
</tr>
</cfoutput>
</table>
<cfinclude template="../system/common/grid_navi.cfm">