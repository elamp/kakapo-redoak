<!--- ********************************************************************* --->
<!--- Kakapo 0.36. Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfcomponent displayname ="kakapo" >

<cfinclude template="../settings.cfm">

<cffunction name="stat" access="remote" returntype="any">
	<cfargument name="id"	required="yes" type="any" />
	<cfargument name="stat" required="yes" type="any" />
	
	<cfif StructKeyExists(cookie,'kakapo_login')>
		<cfquery name="update" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
			update #kakapo.table# set
			status		= #val(arguments.stat)#
			where bugid = #val(arguments.id)#
		</cfquery>
	</cfif>
	
	<cfreturn arguments.stat>
</cffunction>

</cfcomponent>