// ********************************************************************* //
// Kakapo 0.35. Released under the GNU licenses 3                        //
// Saman W Jayasekara (sam@cflove.org) http://cflove.org                 //
// Last Update : August 22, 2011                                         //
// ********************************************************************* //

$("document").ready(function() {
	// set tooltip
	$('.alert').tipsy({fade: true, gravity: 'nw'});
	$('.statbtn, .ecounts img').tipsy({fade: true, gravity: 'w'});
	// Tabs
	if ($('.tabline').length !== 0) {
		$('.tabline div').addClass('tab')
		$('.tabline div:first').css('margin-left','10px')
		$('.tabline div:last').css('margin-right','10px')
		$('.tabline').show()
		$('.tabline div').click(function() {
			$('.tabline .tabsel').removeClass('tabsel').addClass('tab')
			$(this).removeClass('tab').addClass('tabsel')
			$(this).parent().next().children('div:visible').hide()
			$(this).parent().next().children('div:nth-child('+ Number(Number($(this).index())+1) +')').show()
		})
		$('.tabline .tab:first').click()
	}
})

// confirmation box /////////////////
function conf(t,q,l) {
	var $dialog = $('<div></div>')
		.html(q)
		.dialog({resizable: false,autoOpen: false,show: 'blind',hide: 'clip',height:140,modal: true,title : t,
			buttons: {
				'Yes': function() {window.location = l},
				'No': function() {$(this).dialog('close');}
			}
		});
	$dialog.dialog('open');
}

/////////////////////////////////////
// error message                  ///
/////////////////////////////////////
function showmsg(i) {
	$('.alertboxmsg').html( '<img src="images/success.png" align="absmiddle" /> ' + i)
	$('#alertbox').slideDown('slow', function() { setTimeout('hidealertbox()', 5000) });
}

function showerror(i) {
	$('.alertboxmsg').html( '<img src="images/critical.png" align="absmiddle" /> ' + i)
	$('#alertbox').slideDown('slow', function() {});
}

function hidealertbox() {
	$('#alertbox').slideUp('slow', function() { });
}