<!--- ********************************************************************* --->
<!--- Kakapo 1.00 Released under the GNU licenses 3                         --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : Aug 27, 2012                                            --->
<!--- ********************************************************************* --->
<cfimport 
	taglib =	"system/ct"  
	prefix =	"ct">

<!--- ************************************************************ --->
<!--- Login/logout Page. Completly seperate from the rest.         --->
<!--- ************************************************************ --->
<cfinclude template="system/defaults.cfm">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<title>Kakapo : Please Login</title>
</head><body>

<cfparam name="form.username" 	default="">
<cfparam name="form.password" 	default="">
<cfparam name="url.path"		default="">
<cfparam name="url.r"			default="">

<!--- ************************************************************ --->
<!--- Logout                                                       --->
<!--- ************************************************************ --->
<cfswitch expression="#url.path#">
	<cfcase value="logout">
		<cfcookie expires="now" name="kakapo_login">
	</cfcase>
</cfswitch>

<cfif len(form.username) and len(form.password)>
<!--- ************************************************************ --->
<!--- Login Action                                                 --->
<!--- ************************************************************ --->
	
	<!--- Feel free to replace authentication with your own login function --->
	<cfinclude template="login_details.cfm">

	<cfif trim(form.username) eq trim(kakapo.username) and trim(form.password) eq trim(kakapo.password)>
		<cfcookie value="#hash(kakapo.username)##hash(kakapo.password)#" name="kakapo_login" />
		<cfset session.login_fail 		= 0>
		
		<cfif len(url.r)>
			 <cfheader statuscode="302" statustext="Object Temporarily Moved">
			 <cfheader name="location" value="index.cfm?#cfusion_decrypt(url.r,'kakapo')#">
		<cfelse>
			 <cfheader statuscode="302" statustext="Object Temporarily Moved">
			 <cfheader name="location" value="index.cfm">
		</cfif>
		
	<cfelse>
		<cfparam name="session.login_fail" default="0">
		<cfif val(session.login_fail) gte 10>
			<cfset session.error	= "Maximum retry limit exceeded. Session Locked.">
		<cfelse>
			<cfset session.login_fail = session.login_fail + 1>
			<cfif val(session.login_fail)>
				<cfset session.error	= "Username or Password is wrong. [#session.login_fail# of 10 Retry]">
			</cfif>
		</cfif>	
	</cfif>
</cfif>

<!--- ************************************************************ --->
<!--- Login Form                                                   --->
<!--- ************************************************************ --->

<div style="vertical-align:middle; height:100%; width:100%;" align="center">
<div style="position: absolute; left:0px; width:100%; top: 30%; vertical-align: middle; ">
<ct:msg>
<div style="background-color:#F0F0F0; border-bottom:1px solid #CCC; background-image:url(images/loginlinebg.png); background-repeat:repeat-x; background-position:0px 43px">

<div style="text-align:right"><a href="http://cflove.org/2010/11/coldfusion-error-handler.cfm" target="_blank"><img src="images/cflove.png" style="float:right" align="absmiddle" border="0" /></a></div>
<div style="background-color:#CCC"><div style="width:330px; text-align:left; margin-left:auto; margin-right:auto;"><img src="images/kakapo_gray.png" title="Kakapo : Login" align="absmiddle" /></div></div>
<div style="width:350px; padding:5px; text-align:left; margin-left:auto; margin-right:auto;">
<cfform action="login.cfm?r=#url.r#" method="post">
	<div class="formfield">
	<div class="label">User Name</div>
		<div class="field">
		<cfinput type="text" name="Username" value="#form.Username#" required="yes" style="width:200px; font-family:Verdana, Geneva, sans-serif; font-size:11px" message="Please Enter User name">
		</div>
	</div>
	
	<div class="formfield">
		<div class="label">Password</div>
		<div class="field">
		<cfinput type="password" name="Password" value="#form.Password#" required="yes" style="width:200px; font-family:Verdana, Geneva, sans-serif; font-size:11px" message="Please Enter Password">
		</div>
	</div>
	
	<div class="formfield"><div class="label">&nbsp;</div>
		<div class="field"><input title="Click Here to Submit" class="buttong" type="submit" value="Login" name="submit" /></div>
	</div></div>
	
</cfform>

</div>
<div style="background:url(images/loginbgline.png); background-repeat:repeat-x; background-position:top"><img src="images/sdow_btm_lft.png" align="left" /><img src="images/sdow_btm_rite.png" align="right" /><div style="clear:both"></div></div>

</div>
</div>

</body>
</html>