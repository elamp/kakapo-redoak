<cfprocessingdirective pageEncoding="utf-8">

<!--- Data Source Name (Valid and Working DSN Required.)             --->
<cfset kakapo.datasource	= "">
<!--- Data Source User Name (Leave Blank if not applicable)          --->
<cfset kakapo.username		= "">
<!--- Data Source Password (Leave Blank if not applicable)           --->
<cfset kakapo.password		= "">
<!--- Log file storage folder (Required)                             --->
<cfset kakapo.LogFolder		= "">
<!--- Compress Log Files (If Your Hosting Provider does not support CFOBJECT tag, set this to 'No') [Yes,No] --->
<cfset kakapo.compress		= "">

<cfset kakapo.table			= "kakapo">
<cfset kakapo.version		= "1.00">