<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfif not val(total)>
	<div style="padding:10px; font-weight:bold">No Records Found</div>
</cfif>
<div class="navi">
	<cfset pagecount	= Ceiling(total/shown)>
    <cfif total gt 10>

    	<cfoutput>
        <div id="gnavbttnbox" class="gnavbttnbox">
            <select class="gselector" id="gselector" name="gselector" onchange="window.location='index.cfm?shown='+$('##gselector').val()">
            	<option value="10">10</option>
                <cfloop from="20" to="100" step="20" index="i"><cfif i lt total><option <cfif shown eq i>selected="selected"</cfif> value="#i#">#i#</option></cfif></cfloop>
            </select>
            <cfif thispage gt 1>
            	<a href="index.cfm?action=#url.action#&errorid=#url.errorid#&page=#val(thispage-1)#"><img src="images/gbck.gif" style="position:relative; top:3px" border="0" width="14" height="14" /></a>
            </cfif>
            
            <!--- limit button set calculation : Start --->
            <cfset Start = (Ceiling(thispage/10)*10)-9>
            <cfset end	 = (Ceiling(thispage/10)*10)>
            <cfif end gt pagecount>
            	<cfset end = pagecount>
            </cfif>
            <!--- limit button set calculation : Ends  --->
            
            <cfloop from="#Start#" to="#end#" index="i">
                <cfif thispage eq i> <u>#i#</u><cfelse> <a href="index.cfm?action=#url.action#&errorid=#url.errorid#&page=#i#">#i#</a></cfif> <img align="absmiddle" src="images/gdiv.gif" width="2" height="9" />
            </cfloop>
            <cfif pagecount gt thispage>
            	 <a href="index.cfm?action=#url.action#&errorid=#url.errorid#&page=#val(thispage+1)#"><img src="images/gnxt.gif" style="position:relative; top:3px" border="0" width="14" height="14" /></a> 
            </cfif>
            Page <input type="text" id="gpagejump" class="gpagejump" onkeypress="{if (event.keyCode==13) window.location='index.cfm?action=#url.action#&errorid=#url.errorid#&page='+$('##gpagejump').val()}"  name="gpagejump" value="#url.page#" /> of #pagecount#
            (#total# Records)
        &nbsp;</div>
    </cfoutput>
	<cfelse>
    
    </cfif>
    <cfoutput><div id="gnavsrchbox" class="gnavsrchbox">
    <cfif len(url.srch)>
    	<a href="index.cfm?fs=1"><img src="images/gryclose.gif" style="position:relative; top:3px" border="0" width="15" height="15" /></a>
    </cfif>
    <input type="text" class="gsearch" id="gsearch" value="#url.srch#" name="gsearch" onkeypress="{if (event.keyCode==13) window.location='index.cfm?fs=1&srch='+$('##gsearch').val()}" /> <input type="button" onclick="window.location='index.cfm?fs=1&srch='+$('##gsearch').val()" class="gbtn" value="Search" /></div></cfoutput>
</div></div>