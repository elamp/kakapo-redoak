<!--- ********************************************************************* --->
<!--- Kakapo 0.36. Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfif not StructKeyExists(cookie,'kakapo_login')>
	<cfset session.m	= "Session Expired! Please Login.">
	<cflocation addtoken="no" url="login.cfm">
</cfif>