<!--- ********************************************************************* --->
<!--- Kakapo 0.36. Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfparam name="url.action" 			default="">
<cfparam name="url.header" 			default="Yes"> 	<!--- show header - use in popups --->
<cfparam name="cookie.resPerPage"	default="10">   <!--- number of search results per a page --->
<cfparam name="url.page"			default="1"> 	<!--- search result page --->
<cfparam name="url.order" 			default="">		<!--- search result display order --->
<cfparam name="url.dir" 			default="">		<!--- search result display direction --->
<cfparam name="url.srch" 			default="">		<!--- search key --->
<cfparam name="url.shown" 			default="">		<!--- grid : number of resutls --->
<cfparam name="url.id" 				default="">
<cfparam name="url.errorid" 		default="0">