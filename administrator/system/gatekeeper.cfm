<!--- ********************************************************************* --->
<!--- Kakapo 0.36. Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfinclude template="../login_details.cfm">
<cfsilent>
<cfif StructKeyExists(cookie,'kakapo_login') and compare(cookie.kakapo_login,"#hash(kakapo.username)##hash(kakapo.password)#")>
	<cfif len(cgi.QUERY_STRING)>
		<cfset session.msg	= "Session Expired. Please Login.">
	</cfif>
	<cflocation addtoken="no" url="login.cfm?r=#cfusion_encrypt(cgi.QUERY_STRING,'kakapo')#">
</cfif>
</cfsilent>
