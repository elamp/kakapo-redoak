<!--- ********************************************************************* --->
<!--- Kakapo 0.36. Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 15, 2012                                         --->
<!--- ********************************************************************* --->

<cfparam name="url.d" default="">

<!--- ****************************************************** --->
<!--- Validate DS and Find Database Type                     --->
<!--- ****************************************************** --->
<cfif listfirst(server.coldfusion.productversion) gte 8>
	<cftry>
		<cf_dbinfo datasource="#cfusion_decrypt(url.d,settingfile)#">
		<cfcatch>
			<div style="color:#FFF; margin-left:40px; margin-right:40px; font-weight:bold; margin-top:20px; padding:10px; background-color:#ED1E1E; -moz-border-radius: 10px;-webkit-border-radius: 5px; border-radius:5px; -khtml-border-radius: 5px;"> &nbsp; Hint: <cfoutput>#cfcatch.Message#</cfoutput>&nbsp; </div>
			<cfset dbinfo.DATABASE_PRODUCTNAME	= 'Other'>
		</cfcatch>
	</cftry>
<cfelse>
	<cfset dbinfo.DATABASE_PRODUCTNAME	= ''>
</cfif>

<div class="bluebox">
<div class="formblock">
<cfform id="formfield" action="index.cfm?action=4" method="post">
<div class="accordinghead">Page 2 of 3</div>

<cfif len(url.d)>
	<div class="settingitem">
	Database Type
	<br />
	<cfoutput><input type="hidden" name="d" value="#url.d#" /><input type="hidden" name="u" value="#url.u#" /><input type="hidden" name="p" value="#url.p#" /></cfoutput>
	<div style="padding-left:20px; padding-top:5px">
	<input type="radio" value="mssql" name="dbtype" />  MS SQL &nbsp; 
	<input type="radio" value="mysql" name="dbtype" />  My SQL &nbsp;
	<input type="radio" value="pssql" name="dbtype" />  PostgreSQL &nbsp;
	<input type="radio" value="skip" name="dbtype" />  Skip Table Creation &nbsp;
	</div>	
	</div>
	
	<div class="settingitem">
	<strong>Create New Kakapo Login</strong>
	<br /><br />
	User Name <input type="text" style="width:100px" name="username" id="username" value="Admin" class="text" />
	Password <input type="text" style="width:100px" name="password" id="password" class="text" />
	</div>
	
	<div style="padding-left:100px; padding-top:10px">
		<input title="Click Here to Save" class="button" type="submit" value="Continue" name="submit" />
	</div>
<cfelse>
	<strong>Installation failed.</strong>
	<br /><br />Missing DSN Name
</cfif>
</div>
</cfform>

</div>
<script type="text/javascript">
$('#formfield').submit(function() {
	if ( !$('input:radio[name=dbtype]:checked').length ) {
		alert("Please Select Database Type")
		return false
	}
	if ( !$('#password').val() ) {
		alert("Enter Administrator Login Password")
		return false
	}
})
$(document).ready(function() {
switch('<cfoutput>#dbinfo.DATABASE_PRODUCTNAME#</cfoutput>')
{
case 'Microsoft SQL Server':
  $('input[value=mssql]').not('[checked=checked]').attr('checked','checked')
  break;
case 'PostgreSQL':
  $('input[value=pssql]').not('[checked=checked]').attr('checked','checked')
  break;
case 'MySQL':
  $('input[value=mysql]').not('[checked=checked]').attr('checked','checked')
  break;
case 'Other':
  $('input[value=skip]').not('[checked=checked]').attr('checked','checked')
  break;
}

})
</script>