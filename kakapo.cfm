<cfprocessingdirective pageEncoding="utf-8">

<!--- Data Source Name (Valid and Working DSN Required.)             --->
<cfset kakapo.datasource	= "">
<!--- Data Source User Name (Leave Blank if not applicable)          --->
<cfset kakapo.username		= "">
<!--- Data Source Password (Leave Blank if not applicable)           --->
<cfset kakapo.password		= "">
<!--- Log file storage folder (Required)                             --->
<cfset kakapo.LogFolder		= "">
<!--- Compress Log Files (If Your Hosting Provider does not support CFOBJECT tag, set this to 'No') [Yes,No] --->
<cfset kakapo.compress		= "Yes">

<!--- Send to Email Addresses on error (Leave Blank to disable mail notification.) --->
<cfset kakapo.MailTo		= "">
<!--- From Email Address                                             --->
<cfset kakapo.MailForm		= "">
<!--- Mail Subject                                                   --->
<cfset kakapo.MailSubject	= "Web Site Need Your Attention">
<!--- Mail Server Address                                            --->
<cfset kakapo.MailServer	= "">
<!--- Mail Server User Name                                          --->
<cfset kakapo.MailUser		= "">
<!--- Mail Server Password                                           --->
<cfset kakapo.MailPassword	= "">
<!--- SMTP Port                                                      --->
<cfset kakapo.MailPort		= "">
<!--- Send Mail on [All Exceptions,Fresh Exceptions]                 --->
<cfset kakapo.mailon		= "Fresh Exceptions">

<!--- Kakapo Admin URL                                               --->
<cfset kakapo.AdminURL		= "">
<!--- Debug IP Address list (Separated By Comma. These IP address will see the Robust Exception Information and others friendly error page.) --->
<cfset kakapo.homeip		= "">
<!--- Take a Snap Shot of the Error Page Code [Yes,No]               --->
<cfset kakapo.LogPage	 	= "Yes">

<!--- Headline of the Custom Error Message                           --->
<cfset kakapo.title			= "Sorry! Something Just Went Wrong">
<!--- Description of the Custom Error Message                        --->
<cfset kakapo.desc			= "Don't worry though. We're working on getting this fixed as soon as posible.">

<!--- Number of Query Records to save                                                      --->
<cfset kakapo.QueryRecNum		= "50">

<!--- Return HTTP Status Code [Yes,NO]                                                      --->
<cfset kakapo.UseHttpStatus		= "NO">

<!--- Variables to Ignore                                                      --->
<cfset kakapo.ignoreVariablesCustom		= "">







<!--- Above Blank space reserved for future use.                     --->

<!--- ************************************************************** --->
<!--- [SEM] - Added code to add method arguments to variables scope. --->
<!--- ************************************************************** --->
<cfif IsDefined("arguments")>
	<cfset MethodArguments = arguments>
</cfif>


<!--- ************************************************************** --->
<!--- Kakapo 1.00 Released under the GNU licenses 3                  --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org          --->
<!--- Last Update : August 27, 2012                                  --->
<!--- ************************************************************** --->

<!--- ************************************************************** --->
<!--- Fix Settings - Edit With Care                                  --->
<!--- ************************************************************** --->
<cfset kakapo.LogVariables		= 'Yes'>
<cfset kakapo.table				= 'kakapo'>
<cfset kakapo.version			= '1.00'>
<cfset kakapo.ignoreVariables	= "kakapoLog,kakapo_formatSQL,fb_,kakapo_wddxsafe,kakapoBox,error,cferror,kakapo.wddx,cfcatch,kakapoBox.vari,kakapo,kakapo_serialize,cfquery,cfquery.executiontime,kakapo_gzip">
<cfset kakapo.Code				= CreateUUID()>
<cfset kakapo.serVer			= listfirst(server.coldfusion.productversion)>
<!--- copy custom ignores --->
<cfif isDefined('kakapo.ignoreVariablesCustom') AND Len(kakapo.ignoreVariablesCustom) >
	<cfset kakapo.ignoreVariables = listAppend(kakapo.ignoreVariables,kakapo.ignoreVariablesCustom)>
</cfif>

<cfif not StructKeyExists(variables,'cferror') and not StructKeyExists(variables,'cfcatch')>
	<cfabort>
</cfif>
<!--- ************************************************************** --->
<!--- Handle Error Variables                                         --->
<!--- ************************************************************** --->
<cfset kakapoLog				= StructNew()>
<cfsetting requesttimeout="30">

<!--- check for cfcatch or cf error and set to a neautral variable --->
<cfif StructKeyExists(variables,'cferror')>
	<cfset kakapo.RootCause = cferror.RootCause >
<cfelse>
	<cfset kakapo.RootCause = cfcatch >
</cfif>

<cftry>
<cfset kakapoLog.Template		= kakapo.RootCause.TagContext[1].Template>
<cfset kakapoLog.Line			= kakapo.RootCause.TagContext[1].Line>
	<cfif StructKeyExists(variables,'cferror')>
		<cfset kakapoLog.Type			= kakapo.RootCause.TagContext[1].Type>
	<cfelse>
		<cfset kakapoLog.Type			= "(#kakapo.RootCause.TagContext[1].Type#)">
	</cfif>
	<cfcatch>
	<cfset kakapoLog.Template	= 'Unknown'>
	<cfset kakapoLog.Line		= 0>
	<cfset kakapoLog.Type		= 'Error'>
	</cfcatch>
</cftry>

<cfif StructKeyExists(kakapo.RootCause,'NativeErrorCode')>
	<cfset kakapoLog.NativeErrorCode	= kakapo.RootCause.NativeErrorCode>
</cfif>
<cfif StructKeyExists(kakapo.RootCause,'ErrorCode')>
	<cfset kakapoLog.ErrorCode	= kakapo.RootCause.ErrorCode>
</cfif>
<cfif StructKeyExists(kakapo.RootCause,'Sql')>
	<cfset kakapoLog.Sql		= kakapo.RootCause.Sql>
</cfif>
<cfif StructKeyExists(kakapo.RootCause,'where')>
	<cfset kakapoLog.where		= kakapo.RootCause.where>
</cfif>
<cfif StructKeyExists(kakapo.RootCause,'DataSource')>
	<cfset kakapoLog.DataSource	= kakapo.RootCause.DataSource>
</cfif>

<cfif StructKeyExists(kakapo.RootCause,'Message')>
	<cfset kakapoLog.Message	= kakapo.RootCause.Message>
	<cfset kakapoLog.Detail		= kakapo.RootCause.Detail>
	<cfset kakapoLog.StackTrace	= kakapo.RootCause.StackTrace>
<cfelse>
	<cfset kakapoLog.Message	= 'Unknown Error'>
	<cfset kakapoLog.Detail		= 'Error Handling Page Triggered without further details.'>
	<cfset kakapoLog.StackTrace	= ''>
</cfif>

<cfset kakapoLog.DateTime		= now()>
<cfset kakapoLog.RemoteAddress	= cgi.Remote_Addr>
<cfset kakapoLog.HTTPReferer	= cgi.HTTP_REFERER>
<cfset kakapoLog.Browser		= cgi.HTTP_USER_AGENT>

<cftry>
	<cfif ArrayLen(kakapo.RootCause.TagContext) gt 1>
		<cfset kakapoLog.CalledFrom	= ArrayNew(1)>
		<cfloop from="2" to="#val(ArrayLen(kakapo.RootCause.TagContext))#" index="kakapoLog.i">
			<cfset ArrayAppend(kakapoLog.CalledFrom,"<b>Called from</b> #kakapo.RootCause.TagContext[kakapoLog.i].Template#: line #kakapo.RootCause.TagContext[kakapoLog.i].Line#")>
		</cfloop>
		<cfset kakapoLog.CalledFrom	= ArrayToList(kakapoLog.CalledFrom,'<br />')>
	<cfelse>
		<cfset kakapoLog.CalledFrom	= "">
	</cfif>
<cfcatch>
	<cfset kakapoLog.CalledFrom	= "">
</cfcatch>
</cftry>

<cftry>
<cfset kakapoLog.Code	= ''>
<cfif FileExists('#kakapoLog.template#')>
	<cffile action = "read" file = "#kakapoLog.template#" variable = "kakapoLog.Code">
</cfif>
<cfif len(trim(kakapoLog.Code))>
	<!--- converet page code in to array --->
	<cfset kakapoLog.Code	= replace(kakapoLog.Code,chr(10),'#chr(10)# ','all')>
	<cfset kakapoLog.temp	= ArrayNew(1)>
	<cfloop list="#kakapoLog.Code#" delimiters="#chr(10)#" index="kakapoLog.i">
		<cfset ArrayAppend(kakapoLog.temp,kakapoLog.i)>
	</cfloop>
	<cfset kakapoLog.Code	= kakapoLog.temp>
	<cfset StructDelete(kakapoLog,'temp')>
<cfelse>
	<cfset kakapoLog.Code	= ArrayNew(1)>
</cfif>
	<cfcatch>
		<cfset kakapoLog.Code	= ArrayNew(1)>
	</cfcatch>
</cftry>

<cfif val(kakapoLog.line) and ArrayLen(kakapoLog.Code)>
	<cfset kakapoLog.StLine = val(kakapoLog.line)-2>
	<cfif kakapoLog.StLine lt 1>
		<cfset kakapoLog.StLine = 1>
	</cfif>
	<cfset kakapoLog.EdLine = kakapoLog.line + 2>
	<cfif kakapoLog.EdLine gt ArrayLen(kakapoLog.Code)>
		<cfset kakapoLog.EdLine = ArrayLen(kakapoLog.Code)>
	</cfif>
	<cfoutput><cfsavecontent variable="kakapoLog.errorCodeLines"><cfloop from="#kakapoLog.StLine#" to="#kakapoLog.EdLine#" index="kakapoLog.i"><pre style="white-space:-moz-pre-wrap; white-space:-pre-wrap; font-family:Verdana; white-space:-o-pre-wrap; white-space:pre-wrap; word-wrap:break-word; padding:0px; margin:0px; font-size:11px">#kakapoLog.i# : <cfif kakapoLog.i eq kakapoLog.line><b>#HTMLEditFormat(kakapoLog.Code[kakapoLog.i])#</b><cfelse>#HTMLEditFormat(kakapoLog.Code[kakapoLog.i])#</cfif></pre></cfloop></cfsavecontent></cfoutput>
</cfif>

<!--- ************************************************************** --->
<!--- insert basic error details to the database                     --->
<!--- ************************************************************** --->
<cftry>
	<cftransaction>
	<!--- create error id   --->
	<cfquery name="kakapo.i" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
		select errorid from #kakapo.table# where message =
		<cfqueryparam
			value="#left(trim(kakapoLog.Message),500)#"
			cfsqltype="CF_SQL_VARCHAR"
			maxlength="500">
		and template = <cfqueryparam
			value="#left(trim(kakapoLog.template),200)#"
			cfsqltype="CF_SQL_VARCHAR"
			maxlength="200"> and line = #val(kakapoLog.Line)#
	</cfquery>

	<cfif val(kakapo.i.errorid)>
		<cfset kakapo.Errorid	= val(kakapo.i.errorid)>
		<cfswitch expression="#kakapo.mailon#">
			<cfcase value="Fresh Exceptions">
				<cfset kakapo.MailTo	= ''> <!--- do not mail on repeating errors  --->
			</cfcase>
		</cfswitch>
	<cfelse>
		<cfquery name="kakapo.i" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
			select max(errorid)+1 as mxerrorid from #kakapo.table#
		</cfquery>
		<cfset kakapo.Errorid	= val(kakapo.i.mxerrorid)+1>
	</cfif>
	<!--- insert the record --->
	<cfquery datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#" name="kakapo.add">
		insert into #kakapo.table#
		(line, etime, errorid, code, template,message,type)
		values
		(#val(kakapoLog.Line)#,#createODBCdateTime(kakapoLog.DateTime)#,#val(kakapo.Errorid)#,
		<cfqueryparam
			value="#kakapo.Code#"
			cfsqltype="CF_SQL_VARCHAR"
			maxlength="35">,
		<cfqueryparam
			value="#left(trim(kakapoLog.template),200)#"
			cfsqltype="CF_SQL_VARCHAR"
			maxlength="200">,
		<cfqueryparam
			value="#left(trim(kakapoLog.Message),500)#"
			cfsqltype="CF_SQL_VARCHAR"
			maxlength="500">,
		<cfqueryparam
			value="#kakapoLog.Type#"
			cfsqltype="CF_SQL_VARCHAR"
			maxlength="20">
			)
	</cfquery>
	</cftransaction>
	<cfcatch></cfcatch>
</cftry>

<cftry>
<cfif len(kakapo.homeip) and listfind(kakapo.homeip,cgi.remote_addr)>

<!--- ************************************************************** --->
<!--- Mimic ColdFusion Error for Debug IP address                    --->
<!--- ************************************************************** --->
<cfif IsDefined('error.GeneratedContent')><cfoutput>#error.GeneratedContent#</cfoutput></cfif>
<!-- " ---></TD></TD></TD></TH></TH></TH></TR></TR></TR></TABLE></TABLE></TABLE></A></ABBREV></ACRONYM></ADDRESS></APPLET></AU></B></BANNER></BIG></BLINK></BLOCKQUOTE></BQ></CAPTION></CENTER></CITE></CODE></COMMENT></DEL></DFN></DIR></DIV></DL></EM></FIG></FN></FONT></FORM></FRAME></FRAMESET></H1></H2></H3></H4></H5></H6></HEAD></I></INS></KBD></LISTING></MAP></MARQUEE></MENU></MULTICOL></NOBR></NOFRAMES></NOSCRIPT></NOTE></OL></P></PARAM></PERSON></PLAINTEXT></PRE></Q></S></SAMP></SCRIPT></SELECT></SMALL></STRIKE></STRONG></SUB></SUP></TABLE></TD></TEXTAREA></TH></TITLE></TR></TT></U></UL></VAR></WBR></XMP>
<font face="arial"></font><html><head><title>Error Occurred While Processing Request</title></head><body>
<script language="JavaScript">
function showHide(targetName) {
	if( document.getElementById ) { // NS6+
		target = document.getElementById(targetName);
	} else if( document.all ) { // IE4+
		target = document.all[targetName];
	}

	if( target ) {
		if( target.style.display == "none" ) {
			target.style.display = "inline";
		} else {
			target.style.display = "none";
		}
	}
}
</script>
<font style="COLOR: black; FONT: 16pt/18pt verdana">The web site you are accessing has experienced an unexpected error.<br>Please contact the website administrator.</font><br><br>
<div style="COLOR: black; FONT: 8pt/11pt verdana">
<div style="padding:3px; background-color:#E7E7E7; border:1px solid #000808">
<div style="COLOR: white; FONT: 11pt/13pt verdana; padding:4px; background-color:#000066">The following information is meant for the website developer for debugging purposes.</div>
<div style="COLOR: white; FONT: 11pt/13pt verdana; padding:3px; margin-top:4px; margin-bottom:3px; background-color:#4646EE; border:1px solid #000808">Error Occurred While Processing Request</div>
<div style="border:1px solid #000808; padding:3px">
<cfoutput>
<div style="FONT: 13pt/15pt verdana">#replace(kakapoLog.Message,'<','&lt;','all')#</div>
<cfif StructKeyExists(kakapoLog,'detail') and len(trim(kakapoLog.detail))><br/>#kakapoLog.detail#<br></cfif>
	<br />The error occurred in <b>#kakapoLog.Template#:  Line #kakapoLog.line#</b>
<cfif StructKeyExists(kakapoLog,'CalledFrom') and len(trim(kakapoLog.CalledFrom))>#kakapoLog.CalledFrom#</cfif>

<br /><br />#kakapoLog.errorCodeLines#
<br /><hr color="##C0C0C0" noshade />

<cfif StructKeyExists(kakapoLog,'NativeErrorCode')>
	<div style="width:150px; float:left">VENDORERRORCODE</div><div style="float:left">#kakapoLog.NativeErrorCode#</div><div style="clear:both; padding-bottom: 5px;"></div>
</cfif>
<cfif StructKeyExists(kakapoLog,'ErrorCode') and StructKeyExists(kakapoLog,'Sql')>
	<div style="width:150px; float:left">SQLSTATE</div><div style="float:left">#kakapoLog.ErrorCode#</div><div style="clear:both; padding-bottom: 5px;"></div>
</cfif>
<cfif StructKeyExists(kakapoLog,'Sql')>
	<div style="width:150px; float:left">SQL</div><div style="margin-left: 150px;">#kakapoLog.Sql#</div><div style="clear:both; padding-bottom: 5px;"></div>
</cfif>
<cfif StructKeyExists(kakapoLog,'where') and len(kakapoLog.where)>
	<div style="width:150px; float:left">SQL Params</div><div style="margin-left: 150px;">#kakapoLog.where#</div><div style="clear:both; padding-bottom: 5px;"></div>
	<div style="width:150px; float:left">Generated SQL</div><div style="margin-left: 150px;">#kakapo_formatSQL(kakapoLog.Sql,kakapoLog.where)#</div><div style="clear:both; padding-bottom: 5px;"></div>
</cfif>
<cfif StructKeyExists(kakapoLog,'DataSource')>
	<div style="width:150px; float:left">DATASOURCE</div><div style="float:left">#kakapoLog.DataSource#</div><div style="clear:both; padding-bottom: 5px;"></div>
</cfif>
</cfoutput>

Resources:<br />
<ul style=" list-style-position: inside; list-style-type: disc;">
<li>Check the <a href='http://www.macromedia.com/go/proddoc_getdoc' target="new">ColdFusion documentation</a> to verify that you are using the correct syntax.</li>
<li>Search the <a href='http://www.macromedia.com/support/coldfusion/' target="new">Knowledge Base</a> to find a solution to your problem.</li>
</ul>
<cfoutput>
<div style="float:left; clear:left; width:100px">Browser</div><div style="float:left;"><cfif StructKeyExists(kakapoLog,'Browser')>#kakapoLog.Browser#</cfif></div><br />
<div style="float:left; clear:left; width:100px">Remote Address</div><div style="float:left;"><cfif StructKeyExists(kakapoLog,'RemoteAddress')>#kakapoLog.RemoteAddress#</cfif></div><br />
<div style="float:left; clear:left; width:100px">Referrer</div><div style="float:left;"><cfif StructKeyExists(kakapoLog,'HTTPReferer')>#kakapoLog.HTTPReferer#</cfif></div><br />
<div style="float:left; clear:left; width:100px">Date/Time</div><div style="float:left;"><cfif StructKeyExists(kakapoLog,'DateTime')>#dateformat(kakapoLog.DateTime,'dd-mmm-yy')# #timeformat(kakapoLog.DateTime,'hh:mm:ss tt')#</cfif></div>
<div style="clear:both"></div><br />
<cfif len(kakapoLog.StackTrace)>
	<a href="javascript:showHide('stackbox')">Stack Trace (click to expand)</a>
	<div id="stackbox" style="display:none">
	<pre style="white-space: -moz-pre-wrap; white-space: -pre-wrap;white-space: -o-pre-wrap;white-space: pre-wrap;word-wrap: break-word;">#kakapoLog.StackTrace#</pre>
	</div>
</cfif>
&nbsp;
</div></div></div>
<font style="font-size:11px; color:##060; font-family:Verdana, Geneva, sans-serif">Kakapo #kakapo.version#</font>
</cfoutput>
<cfelse>
<!--- ************************************************************** --->
<!--- Public see the friendly error message.                         --->
<!--- ************************************************************** --->
<cfif StructKeyExists(variables,'cferror')>  <!--- don't show if cfcatch --->
<cfif YesNoFormat(kakapo.UseHttpStatus)>
	<cfheader statusCode="500" statusText="#kakapoLog.Message#">
</cfif>

<div align="center">
<div style="margin-top:100px; width:600px; color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-style:14px;">
<div style="padding:10px; width:600px; background-color:#666; width:600px;  text-align:center"><cfoutput>#kakapo.title#</cfoutput></div>
<div style="width:600px; padding:10px; background-color:#74AA32; text-align:left"><cfoutput>#kakapo.desc#</cfoutput><br /> <br /></div></div></div>
</cfif>

<!--- ************************************************************** --->
<!--- Send Email Notification                                        --->
<!--- ************************************************************** --->
	<cftry>
	<cfif len(kakapo.MailTo)>
	<cfif len(kakapo.MailServer)>
		<cfmail
			to		= "#kakapo.MailTo#"
			from	= "#kakapo.MailForm#"
			subject	= "#kakapo.MailSubject#"
			type	= "html"
			server	= "#kakapo.MailServer#"
			username= "#kakapo.MailUser#"
			password= "#kakapo.MailPassword#"
			port	= "#kakapo.MailPort#">
			<br /><a href="#kakapo.AdminURL#/index.cfm?action=incidence&errorid=#kakapo.Errorid#">Kakapo Just Eat a Bug in Your Web Site</a><br /><br />
			<cfif StructKeyExists(kakapoLog,'Message')>Message : <font face="Courier New, Courier, monospace">#kakapoLog.Message#</font><br /></cfif>
			<cfif StructKeyExists(kakapoLog,'Template')>Template : <font face="Courier New, Courier, monospace">#kakapoLog.Template#<br /></font></cfif>
			<cfif StructKeyExists(kakapoLog,'DateTime')>Time : <font face="Courier New, Courier, monospace">#dateformat(kakapoLog.DateTime,'dddd, mmmm dd yyyy')# #timeformat(kakapoLog.DateTime,'hh:mm ss l TT')#</font><br /></cfif>
			<cfif StructKeyExists(kakapoLog,'Detail') and len(kakapoLog.Detail)>Detail : <font face="Courier New, Courier, monospace">#kakapoLog.Detail#<br /></font></cfif>
		</cfmail>
	<cfelse>
		<cfmail
			to		= "#kakapo.MailTo#"
			from	= "#kakapo.MailForm#"
			subject	= "#kakapo.MailSubject#"
			type	= "html">
			<br /><a href="#kakapo.AdminURL#/index.cfm?action=incidence&errorid=#kakapo.Errorid#">Kakapo Just Eat a Bug in Your Web Site</a><br /><br />
			<cfif StructKeyExists(kakapoLog,'Message')>Message : <font face="Courier New, Courier, monospace">#kakapoLog.Message#</font><br /></cfif>
			<cfif StructKeyExists(kakapoLog,'Template')>Template : <font face="Courier New, Courier, monospace">#kakapoLog.Template#<br /></font></cfif>
			<cfif StructKeyExists(kakapoLog,'DateTime')>Time : <font face="Courier New, Courier, monospace">#dateformat(kakapoLog.DateTime,'dddd, mmmm dd yyyy')# #timeformat(kakapoLog.DateTime,'hh:mm ss l TT')#</font><br /></cfif>
			<cfif StructKeyExists(kakapoLog,'Detail') and len(kakapoLog.Detail)>Detail : <font face="Courier New, Courier, monospace">#kakapoLog.Detail#<br /></font></cfif>
		</cfmail>
	</cfif>
	</cfif>
		<cfcatch></cfcatch>
	</cftry>

</cfif>
	<cfcatch></cfcatch>
</cftry>

<!--- ************************************************************** --->
<!--- Take Snapshot of the code                                      --->
<!--- ************************************************************** --->
<cfif YesNoFormat(kakapo.LogPage)>
	<cftry>
		<cffile
			action 	= "write"
			file 	= "#kakapo.LogFolder#/pages/#kakapo.Code#.cfm"
			output 	= "<cfabort />#kakapo_gzip(ArrayToList(kakapoLog.Code,chr(10)) )#">
		<cfcatch></cfcatch>
	</cftry>
	<cfset StructDelete(kakapoLog,'Code')>
</cfif>

<!--- ************************************************************** --->
<!--- Save Variables                                                 --->
<!--- ************************************************************** --->
<cfif YesNoFormat(kakapo.LogVariables)>

	<cfset kakapoBox 					= StructNew()>
	<cfset kakapoBox.vari.kakapoLog  	= kakapoLog>

	<!--- Variable Tracking --->
	<cfloop collection="#variables#" item="kakapo.i">
	<cftry>
		<cfif not listfindnocase(kakapo.ignoreVariables,kakapo.i)>
			<cfif IsQuery(variables[kakapo.i])>
				<cfquery name="kakapo.TempQ" dbtype="query" maxrows="#kakapo.QueryRecNum#">
					select *
					from #kakapo.i#
				</cfquery>
				<cfset SetVariable("kakapoBox.query.variables_#kakapo.i#",kakapo.TempQ)>
			<cfelse>
				<cfset SetVariable("kakapoBox.vari.#kakapo.i#",variables[kakapo.i])>
			</cfif>

		</cfif>
		<cfcatch></cfcatch>
	</cftry>
	</cfloop>
	<!--- Session Tracking --->
	<cfloop collection="#session#" item="kakapo.i">
	<cftry>
		<cfif not listfindnocase(kakapo.ignoreVariables,kakapo.i)>
			<cfif IsQuery(session[kakapo.i])>
				<cfquery name="kakapo.TempQ" dbtype="query" maxrows="#kakapo.QueryRecNum#">
					select *
					from #kakapo.i#
				</cfquery>
				<cfset SetVariable("kakapoBox.session.session_#kakapo.i#",kakapo.TempQ)>
			<cfelse>
				<cfset SetVariable("kakapoBox.session.#kakapo.i#",duplicate(session[kakapo.i]))>
			</cfif>
		</cfif>
		<cfcatch></cfcatch>
	</cftry>
	</cfloop>
	<!--- Request Tracking --->
	<cfloop collection="#request#" item="kakapo.i">
	<cftry>
		<cfif not listfindnocase(kakapo.ignoreVariables,kakapo.i)>
			<cfif IsQuery(request[kakapo.i])>
				<cfquery name="kakapo.TempQ" dbtype="query" maxrows="#kakapo.QueryRecNum#">
					select *
					from #kakapo.i#
				</cfquery>
				<cfset SetVariable("kakapoBox.request.request_#kakapo.i#",kakapo.TempQ)>
			<cfelse>
				<cfset SetVariable("kakapoBox.request.#kakapo.i#",request[kakapo.i])>
			</cfif>
		</cfif>
		<cfcatch></cfcatch>
	</cftry>
	</cfloop>

	<cftry>
		<cfif IsDefined('CGI')>
			<cfset kakapoBox.cgi	= cgi>
		</cfif>
	<cfcatch></cfcatch>
	</cftry>
	<cftry>
		<cfif IsDefined('URL')>
			<cfset kakapoBox.url	= url>
		</cfif>
	<cfcatch></cfcatch>
	</cftry>
	<cftry>
		<cfif IsDefined('form')>
			<cfset kakapoBox.form	= form>
		</cfif>
	<cfcatch></cfcatch>
	</cftry>
	<cftry>
		<cfif IsDefined('cookie')>
			<cfset kakapoBox.cookie	= duplicate(cookie)>
		</cfif>
	<cfcatch></cfcatch>
	</cftry>

	<cfloop collection="#kakapoBox#" item="kakapo.i">
		<cfset kakapo_wddxsafe('kakapoBox',kakapo.i,kakapoBox[kakapo.i])>
	</cfloop>
	<cfwddx action="cfml2wddx" input="#kakapoBox#" output="kakapo.wddx">
	<cftry>
		<cffile
			action 		= "write"
			file 		= "#kakapo.LogFolder#/variables/#kakapo.Code#.cfm"
			output 		= "<cfabort />#kakapo_gzip(kakapo.wddx)#">
		<cfcatch></cfcatch>
	</cftry>
</cfif>

<!--- ************************************************************** --->
<!--- Helpers                                                        --->
<!--- ************************************************************** --->

<!--- http://www.cflib.org/udf/gzip --->
<cffunction name="kakapo_gzip"
    returntype	="any"
    displayname	="gzip"
    hint		="compresses a string using the gzip algorithm; returns binary or string(base64|hex|uu)"
    output		="no">
	<cfargument name="string" 	required="no" 	type="string" default="">
	<cfargument name="encode" 	required="no" 	type="string" default="base64">

	<cfif YesNoFormat(kakapo.compress)>
		<cftry>
			<cfset local					= StructNew()>
			<cfset local.text				= createObject("java","java.lang.String").init(arguments.string)>
			<cfset local.dataStream			= createObject("java","java.io.ByteArrayOutputStream").init()>
			<cfset local.compressDataStream	= createObject("java","java.util.zip.GZIPOutputStream").init(local.dataStream)>
			<cfset local.compressDataStream.write(local.text.getBytes())>
			<cfset local.compressDataStream.finish()>
			<cfset local.compressDataStream.close()>
			<cfset kakapo.i				= local.dataStream.toByteArray()>
			<cfset StructDelete(variables,'local')>
			<cfreturn binaryEncode(kakapo.i,arguments.encode)>
			<cfcatch>
				<cfreturn arguments.string>
			</cfcatch>
		</cftry>
	<cfelse>
		<cfreturn cfusion_encrypt(arguments.string,kakapo.Code)>
	</cfif>
</cffunction>

<!--- cfwddx does not work same across all server versions   --->
<cffunction name= "kakapo_wddxsafe"
    returntype	= "any"
    hint		= "Remove CFWDDX unsafe data from data structure"
    output		= "Yes">
	<cfargument name="root"		required="Yes" 	type="any" default="">
	<cfargument name="name" 	required="Yes" 	type="any" default="">
	<cfargument name="value" 	required="Yes" 	type="any" default="">

	<cfset local = StructNew()>
	<cftry>
		<!--- isXML() fail on a structure / workaround needed--->
		<cfset local.thisxml	= isXML(value)>
		<cfcatch><cfset local.thisxml	= 'No'></cfcatch>
	</cftry>
	<cfif YesNoFormat(local.thisxml)>
		<cfset SetVariable("#arguments.root#.#arguments.name#",ToString(arguments.value))>
	<cfelseif StructKeyExists(GetFunctionList(),'IsImage') and IsImage(arguments.value)>
		<cfset SetVariable("#arguments.root#.#arguments.name#",ImageInfo(arguments.value))>
	<cfelseif StructKeyExists(GetFunctionList(),'IsPDFObject') and IsPDFObject(arguments.value)>
		<cftry>
		<cfset SetVariable("#arguments.root#.#arguments.name#",getMetaData(arguments.value))>
			<cfcatch>
				<cfset SetVariable("#arguments.root#.#arguments.name#",'[PDF Object]')>
			</cfcatch>
		</cftry>
	<cfelseif IsObject(arguments.value) or IsCustomFunction(arguments.value)>
		<cfset SetVariable("#arguments.root#.#arguments.name#",getMetaData(arguments.value))>
	<cfelseif IsBinary(arguments.value)>
		<cfset SetVariable("#arguments.root#.#arguments.name#",'[Binary Object]')>
	<cfelseif IsStruct(arguments.value) and val(StructCount(arguments.value))>
		<!--- go in the next level and validate variables --->
		<cfloop collection="#arguments.value#" item="local.i">
			<cfset kakapo_wddxsafe('#arguments.root#.#arguments.name#',local.i,arguments.value[local.i])>
		</cfloop>
	</cfif>
	<cfset StructDelete(variables,'local')>
</cffunction>

<!--- takes the sql statement and the cfqueryparams and generates a sql statement that can be cut and pasted into a query editor.(Credit: Lamp, Ed) --->
<cffunction name="kakapo_formatSQL"   returntype	= "string"  hint= "I take a sql queru and params and merge the 2"   output= "NO">
	<cfargument name="SQL"		required="Yes" 	type="string" default="">
	<cfargument name="Params" 	required="Yes" 	type="string" default="">
    <cfset local.count= 1>
    <cfset local.localSQL 	= arguments.Sql>
    <!--- loop over params - treat as a list --->
    <cfloop list="#arguments.Params#" index="local.j" delimiters="]">
    	<!--- find the value --->
        <cfset local.st		= ReFindNoCase("value=\'([^\']*)\'",local.j, 1, "True")>
        <!--- make sure the regex found a value --->
        <cfif local.st.Pos[1] NEQ 0>
        	<!--- grab the value --->
            <cfset local.val		= mid(local.j,local.st.Pos[2],local.st.len[2])>
            <!--- if not a number than put single quotes arount it --->
            <cfif not IsNumeric(local.val)>
            	<cfset local.val	= "'#local.val#'">
            </cfif>
            <!--- replace the placeholder in the sql statement with the actual value --->
        	<cfset local.localSQL 	= replacenoCase(local.localSQL, "(param #local.count#)", local.val )>
        </cfif>
        <cfset local.count	= local.count + 1>
    </cfloop>

	<cfreturn local.localSQL >
	<cfset StructDelete(variables,'local')>
</cffunction>

<cfset StructDelete(variables,'kakapo_formatSQL')>
<cfset StructDelete(variables,'kakapo_wddxsafe')>
<cfset StructDelete(variables,'kakapo_gzip')>